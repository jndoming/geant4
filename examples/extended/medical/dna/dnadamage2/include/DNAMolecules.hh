//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
// This example is provided by the Geant4-DNA collaboration
// DNADAMAGE2 example is derived from the chem6 example
// chem6 example authors: W. G. Shin and S. Incerti (CENBG, France)
//
// The code is developed in the framework of the ESA AO7146
//
// We would be very happy hearing from you, send us your feedback! :)
//
// In order for Geant4-DNA to be maintained and still open-source,
// article citations are crucial. 
// If you use Geant4-DNA chemistry and you publish papers about your software, 
// in addition to the general paper on Geant4-DNA:
//
// Int. J. Model. Simul. Sci. Comput. 1 (2010) 157–178
//
// we would be very happy if you could please also cite the following
// reference papers on chemistry:
//
// Authors: J. Naoki D. Kondo (UCSF, US)
//      J. Ramos-Mendez and B. Faddegon (UCSF, US)
//
// J. Comput. Phys. 274 (2014) 841-882
// Prog. Nucl. Sci. Tec. 2 (2011) 503-508 
/// \file DNAMolecules.hh
/// \brief Definition of the additional DNA molecules

#ifndef DNADAMAGE2_DNAMolecules_h
#define DNADAMAGE2_DNAMolecules_h 1

#include "globals.hh"
#include "G4ios.hh"
#include "G4ParticleDefinition.hh"
#include "G4MoleculeDefinition.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class G4DNA_Adenine : public G4MoleculeDefinition
{
private:
  static /*G4ThreadLocal*/ G4DNA_Adenine* fAdenineInstance;
  G4DNA_Adenine() {;}
  ~G4DNA_Adenine() override {;}

public:
  static G4DNA_Adenine* Definition();
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class G4DNA_DamagedAdenine : public G4MoleculeDefinition
{
private:
  static /*G4ThreadLocal*/ G4DNA_DamagedAdenine* fDamagedAdenineInstance;
  G4DNA_DamagedAdenine() {;}
  ~G4DNA_DamagedAdenine() override {;}

public:
  static G4DNA_DamagedAdenine* Definition();
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class G4DNA_Thymine : public G4MoleculeDefinition
{
private:
  static /*G4ThreadLocal*/ G4DNA_Thymine* fThymineInstance;
  G4DNA_Thymine() {;}
  ~G4DNA_Thymine() override {;}

public:
  static G4DNA_Thymine* Definition();
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class G4DNA_DamagedThymine : public G4MoleculeDefinition
{
private:
  static /*G4ThreadLocal*/ G4DNA_DamagedThymine* fDamagedThymineInstance;
  G4DNA_DamagedThymine() {;}
  ~G4DNA_DamagedThymine() override {;}

public:
  static G4DNA_DamagedThymine* Definition();
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class G4DNA_Cytosine : public G4MoleculeDefinition
{
private:
  static /*G4ThreadLocal*/ G4DNA_Cytosine* fCytosineInstance;
  G4DNA_Cytosine() {;}
  ~G4DNA_Cytosine() override {;}

public:
  static G4DNA_Cytosine* Definition();
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class G4DNA_DamagedCytosine : public G4MoleculeDefinition
{
private:
  static /*G4ThreadLocal*/ G4DNA_DamagedCytosine* fDamagedCytosineInstance;
  G4DNA_DamagedCytosine() {;}
  ~G4DNA_DamagedCytosine() override {;}

public:
  static G4DNA_DamagedCytosine* Definition();
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class G4DNA_Guanine : public G4MoleculeDefinition
{
private:
  static /*G4ThreadLocal*/ G4DNA_Guanine* fGuanineInstance;
  G4DNA_Guanine() {;}
  ~G4DNA_Guanine() override {;}

public:
  static G4DNA_Guanine* Definition();
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class G4DNA_DamagedGuanine : public G4MoleculeDefinition
{
private:
  static /*G4ThreadLocal*/ G4DNA_DamagedGuanine* fDamagedGuanineInstance;
  G4DNA_DamagedGuanine() {;}
  ~G4DNA_DamagedGuanine() override {;}

public:
  static G4DNA_DamagedGuanine* Definition();
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class G4DNA_Deoxyribose : public G4MoleculeDefinition
{
private:
  static /*G4ThreadLocal*/ G4DNA_Deoxyribose* fDeoxyriboseInstance;
  G4DNA_Deoxyribose() {;}
  ~G4DNA_Deoxyribose() override {;}

public:
  static G4DNA_Deoxyribose* Definition();
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class G4DNA_DamagedDeoxyriboseOH : public G4MoleculeDefinition
{
private:
  static /*G4ThreadLocal*/ G4DNA_DamagedDeoxyriboseOH* fDamagedDeoxyriboseOHInstance;
  G4DNA_DamagedDeoxyriboseOH() {;}
  ~G4DNA_DamagedDeoxyriboseOH() override {;}

public:
  static G4DNA_DamagedDeoxyriboseOH* Definition();
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class G4DNA_DamagedDeoxyriboseH : public G4MoleculeDefinition
{
private:
  static /*G4ThreadLocal*/ G4DNA_DamagedDeoxyriboseH* fDamagedDeoxyriboseHInstance;
  G4DNA_DamagedDeoxyriboseH() {;}
  ~G4DNA_DamagedDeoxyriboseH() override {;}

public:
  static G4DNA_DamagedDeoxyriboseH* Definition();
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class G4DNA_DamagedDeoxyriboseEAQ : public G4MoleculeDefinition
{
private:
  static /*G4ThreadLocal*/ G4DNA_DamagedDeoxyriboseEAQ* fDamagedDeoxyriboseEAQInstance;
  G4DNA_DamagedDeoxyriboseEAQ() {;}
  ~G4DNA_DamagedDeoxyriboseEAQ() override {;}

public:
  static G4DNA_DamagedDeoxyriboseEAQ* Definition();
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
