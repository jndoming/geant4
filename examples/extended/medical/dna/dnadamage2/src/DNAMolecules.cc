//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
// Authors: J. Naoki D. Kondo (UCSF, US)
//          J. Ramos-Mendez and B. Faddegon (UCSF, US)
//
// History:
// -----------
// 10 October 2021 J. Naoki D. Kondo created
//
// -------------------------------------------------------------------

#include "DNAMolecules.hh"
#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"
#include "G4ParticleTable.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....
// Adenine

G4DNA_Adenine* G4DNA_Adenine::fAdenineInstance = 0;
G4DNA_Adenine* G4DNA_Adenine::Definition()
{
  if (fAdenineInstance != 0) return fAdenineInstance;
  const G4String name = "DNA_Adenine";

  G4ParticleTable* pTable = G4ParticleTable::GetParticleTable();
  G4ParticleDefinition* anInstance = pTable->FindParticle(name);
  if (anInstance == 0) {
    const G4String formatedName = "DNA_A^{0}";

    G4double mass = 31.99546 * g / Avogadro * c_squared;
    anInstance = new G4MoleculeDefinition(name, mass, 1e-150 * (m * m / s), 0, 0,
                                          1.7 * angstrom, 2);

    ((G4MoleculeDefinition*) anInstance)->SetLevelOccupation(0);
    ((G4MoleculeDefinition*) anInstance)->SetFormatedName(formatedName);
  }
  fAdenineInstance = reinterpret_cast<G4DNA_Adenine*>(anInstance);
  return fAdenineInstance;
}

G4DNA_DamagedAdenine* G4DNA_DamagedAdenine::fDamagedAdenineInstance = 0;
G4DNA_DamagedAdenine* G4DNA_DamagedAdenine::Definition()
{
  if (fDamagedAdenineInstance != 0) return fDamagedAdenineInstance;
  const G4String name = "DNA_DamagedAdenine";

  G4ParticleTable* pTable = G4ParticleTable::GetParticleTable();
  G4ParticleDefinition* anInstance = pTable->FindParticle(name);
  if (anInstance == 0) {
    const G4String formatedName = "DNA_DA^{0}";

    G4double mass = 31.99546 * g / Avogadro * c_squared;
    anInstance = new G4MoleculeDefinition(name, mass, 1e-150 * (m * m / s), 0, 0,
                                          1.7 * angstrom, 2);

    ((G4MoleculeDefinition*) anInstance)->SetLevelOccupation(0);
    ((G4MoleculeDefinition*) anInstance)->SetFormatedName(formatedName);
  }
  fDamagedAdenineInstance = reinterpret_cast<G4DNA_DamagedAdenine*>(anInstance);
  return fDamagedAdenineInstance;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....
// Thymine

G4DNA_Thymine* G4DNA_Thymine::fThymineInstance = 0;
G4DNA_Thymine* G4DNA_Thymine::Definition()
{
  if (fThymineInstance != 0) return fThymineInstance;
  const G4String name = "DNA_Thymine";

  G4ParticleTable* pTable = G4ParticleTable::GetParticleTable();
  G4ParticleDefinition* anInstance = pTable->FindParticle(name);
  if (anInstance == 0) {
    const G4String formatedName = "DNA_T^{0}";

    G4double mass = 31.99546 * g / Avogadro * c_squared;
    anInstance = new G4MoleculeDefinition(name, mass, 1e-150 * (m * m / s), 0, 0,
                                          1.7 * angstrom, 2);

    ((G4MoleculeDefinition*) anInstance)->SetLevelOccupation(0);
    ((G4MoleculeDefinition*) anInstance)->SetFormatedName(formatedName);
  }
  fThymineInstance = reinterpret_cast<G4DNA_Thymine*>(anInstance);
  return fThymineInstance;
}

G4DNA_DamagedThymine* G4DNA_DamagedThymine::fDamagedThymineInstance = 0;
G4DNA_DamagedThymine* G4DNA_DamagedThymine::Definition()
{
  if (fDamagedThymineInstance != 0) return fDamagedThymineInstance;
  const G4String name = "DNA_DamagedThymine";

  G4ParticleTable* pTable = G4ParticleTable::GetParticleTable();
  G4ParticleDefinition* anInstance = pTable->FindParticle(name);
  if (anInstance == 0) {
    const G4String formatedName = "DNA_DT^{0}";

    G4double mass = 31.99546 * g / Avogadro * c_squared;
    anInstance = new G4MoleculeDefinition(name, mass, 1e-150 * (m * m / s), 0, 0,
                                          1.7 * angstrom, 2);

    ((G4MoleculeDefinition*) anInstance)->SetLevelOccupation(0);
    ((G4MoleculeDefinition*) anInstance)->SetFormatedName(formatedName);
  }
  fDamagedThymineInstance = reinterpret_cast<G4DNA_DamagedThymine*>(anInstance);
  return fDamagedThymineInstance;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....
// Cytosine

G4DNA_Cytosine* G4DNA_Cytosine::fCytosineInstance = 0;
G4DNA_Cytosine* G4DNA_Cytosine::Definition()
{
  if (fCytosineInstance != 0) return fCytosineInstance;
  const G4String name = "DNA_Cytosine";

  G4ParticleTable* pTable = G4ParticleTable::GetParticleTable();
  G4ParticleDefinition* anInstance = pTable->FindParticle(name);
  if (anInstance == 0) {
    const G4String formatedName = "DNA_C^{0}";

    G4double mass = 31.99546 * g / Avogadro * c_squared;
    anInstance = new G4MoleculeDefinition(name, mass, 1e-150 * (m * m / s), 0, 0,
                                          1.7 * angstrom, 2);

    ((G4MoleculeDefinition*) anInstance)->SetLevelOccupation(0);
    ((G4MoleculeDefinition*) anInstance)->SetFormatedName(formatedName);
  }
  fCytosineInstance = reinterpret_cast<G4DNA_Cytosine*>(anInstance);
  return fCytosineInstance;
}

G4DNA_DamagedCytosine* G4DNA_DamagedCytosine::fDamagedCytosineInstance = 0;
G4DNA_DamagedCytosine* G4DNA_DamagedCytosine::Definition()
{
  if (fDamagedCytosineInstance != 0) return fDamagedCytosineInstance;
  const G4String name = "DNA_DamagedCytosine";

  G4ParticleTable* pTable = G4ParticleTable::GetParticleTable();
  G4ParticleDefinition* anInstance = pTable->FindParticle(name);
  if (anInstance == 0) {
    const G4String formatedName = "DNA_DC^{0}";

    G4double mass = 31.99546 * g / Avogadro * c_squared;
    anInstance = new G4MoleculeDefinition(name, mass, 1e-150 * (m * m / s), 0, 0,
                                          1.7 * angstrom, 2);

    ((G4MoleculeDefinition*) anInstance)->SetLevelOccupation(0);
    ((G4MoleculeDefinition*) anInstance)->SetFormatedName(formatedName);
  }
  fDamagedCytosineInstance = reinterpret_cast<G4DNA_DamagedCytosine*>(anInstance);
  return fDamagedCytosineInstance;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....
// Guanine

G4DNA_Guanine* G4DNA_Guanine::fGuanineInstance = 0;
G4DNA_Guanine* G4DNA_Guanine::Definition()
{
  if (fGuanineInstance != 0) return fGuanineInstance;
  const G4String name = "DNA_Guanine";

  G4ParticleTable* pTable = G4ParticleTable::GetParticleTable();
  G4ParticleDefinition* anInstance = pTable->FindParticle(name);
  if (anInstance == 0) {
    const G4String formatedName = "DNA_G^{0}";

    G4double mass = 31.99546 * g / Avogadro * c_squared;
    anInstance = new G4MoleculeDefinition(name, mass, 1e-150 * (m * m / s), 0, 0,
                                          1.7 * angstrom, 2);

    ((G4MoleculeDefinition*) anInstance)->SetLevelOccupation(0);
    ((G4MoleculeDefinition*) anInstance)->SetFormatedName(formatedName);
  }
  fGuanineInstance = reinterpret_cast<G4DNA_Guanine*>(anInstance);
  return fGuanineInstance;
}

G4DNA_DamagedGuanine* G4DNA_DamagedGuanine::fDamagedGuanineInstance = 0;
G4DNA_DamagedGuanine* G4DNA_DamagedGuanine::Definition()
{
  if (fDamagedGuanineInstance != 0) return fDamagedGuanineInstance;
  const G4String name = "DNA_DamagedGuanine";

  G4ParticleTable* pTable = G4ParticleTable::GetParticleTable();
  G4ParticleDefinition* anInstance = pTable->FindParticle(name);
  if (anInstance == 0) {
    const G4String formatedName = "DNA_DG^{0}";

    G4double mass = 31.99546 * g / Avogadro * c_squared;
    anInstance = new G4MoleculeDefinition(name, mass, 1e-150 * (m * m / s), 0, 0,
                                          1.7 * angstrom, 2);

    ((G4MoleculeDefinition*) anInstance)->SetLevelOccupation(0);
    ((G4MoleculeDefinition*) anInstance)->SetFormatedName(formatedName);
  }
  fDamagedGuanineInstance = reinterpret_cast<G4DNA_DamagedGuanine*>(anInstance);
  return fDamagedGuanineInstance;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....
// Deoxyribose

G4DNA_Deoxyribose* G4DNA_Deoxyribose::fDeoxyriboseInstance = 0;
G4DNA_Deoxyribose* G4DNA_Deoxyribose::Definition()
{
  
  if (fDeoxyriboseInstance != 0) return fDeoxyriboseInstance;
  const G4String name = "DNA_Deoxyribose";

  G4ParticleTable* pTable = G4ParticleTable::GetParticleTable();
  G4ParticleDefinition* anInstance = pTable->FindParticle(name);
  if (anInstance == 0) {
    const G4String formatedName = "DNA_Deoxy^{0}";

    G4double mass = 31.99546 * g / Avogadro * c_squared;
    anInstance = new G4MoleculeDefinition(name, mass, 1e-150 * (m * m / s), 0, 0,
                                          1.7 * angstrom, 2);

    ((G4MoleculeDefinition*) anInstance)->SetLevelOccupation(0);
    ((G4MoleculeDefinition*) anInstance)->SetFormatedName(formatedName);
  }
  fDeoxyriboseInstance = reinterpret_cast<G4DNA_Deoxyribose*>(anInstance);
  return fDeoxyriboseInstance;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....
// OH_Damamged_Deoxyribose

G4DNA_DamagedDeoxyriboseOH* 
  G4DNA_DamagedDeoxyriboseOH::fDamagedDeoxyriboseOHInstance = 0;
G4DNA_DamagedDeoxyriboseOH* G4DNA_DamagedDeoxyriboseOH::Definition()
{
  if (fDamagedDeoxyriboseOHInstance != 0) return fDamagedDeoxyriboseOHInstance;
  const G4String name = "DNA_DamagedDeoxyriboseOH";

  G4ParticleTable* pTable = G4ParticleTable::GetParticleTable();
  G4ParticleDefinition* anInstance = pTable->FindParticle(name);
  if (anInstance == 0) {
    const G4String formatedName = "DamagedDeoxyriboseOH^{0}";

    G4double mass = 31.99546 * g / Avogadro * c_squared;
    anInstance = new G4MoleculeDefinition(name, mass, 1e-150 * (m * m / s), 0, 0,
                                          1.7 * angstrom, 2);

    ((G4MoleculeDefinition*) anInstance)->SetLevelOccupation(0);
    ((G4MoleculeDefinition*) anInstance)->SetFormatedName(formatedName);
  }
  fDamagedDeoxyriboseOHInstance = 
    reinterpret_cast<G4DNA_DamagedDeoxyriboseOH*>(anInstance);
  return fDamagedDeoxyriboseOHInstance;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....
// H_Damamged_Deoxyribose

G4DNA_DamagedDeoxyriboseH* 
  G4DNA_DamagedDeoxyriboseH::fDamagedDeoxyriboseHInstance = 0;
G4DNA_DamagedDeoxyriboseH* G4DNA_DamagedDeoxyriboseH::Definition()
{
  if (fDamagedDeoxyriboseHInstance != 0) return fDamagedDeoxyriboseHInstance;
  const G4String name = "DNA_DamagedDeoxyriboseH";

  G4ParticleTable* pTable = G4ParticleTable::GetParticleTable();
  G4ParticleDefinition* anInstance = pTable->FindParticle(name);
  if (anInstance == 0) {
    const G4String formatedName = "DamagedDeoxyriboseH^{0}";

    G4double mass = 31.99546 * g / Avogadro * c_squared;
    anInstance = new G4MoleculeDefinition(name, mass, 1e-150 * (m * m / s), 0, 0,
                                          1.7 * angstrom, 2);

    ((G4MoleculeDefinition*) anInstance)->SetLevelOccupation(0);
    ((G4MoleculeDefinition*) anInstance)->SetFormatedName(formatedName);
  }
  fDamagedDeoxyriboseHInstance = 
    reinterpret_cast<G4DNA_DamagedDeoxyriboseH*>(anInstance);
  return fDamagedDeoxyriboseHInstance;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....
// Eaq_Damamged_Deoxyribose

G4DNA_DamagedDeoxyriboseEAQ* 
  G4DNA_DamagedDeoxyriboseEAQ::fDamagedDeoxyriboseEAQInstance = 0;
G4DNA_DamagedDeoxyriboseEAQ* G4DNA_DamagedDeoxyriboseEAQ::Definition()
{
  if (fDamagedDeoxyriboseEAQInstance != 0) return fDamagedDeoxyriboseEAQInstance;
  const G4String name = "DNA_DamagedDeoxyriboseEAQ";
  // search in particle table]
  G4ParticleTable* pTable = G4ParticleTable::GetParticleTable();
  G4ParticleDefinition* anInstance = pTable->FindParticle(name);
  if (anInstance == 0) {
    const G4String formatedName = "DamagedDeoxyriboseEAQ^{0}";

    G4double mass = 31.99546 * g / Avogadro * c_squared;
    anInstance = new G4MoleculeDefinition(name, mass, 1e-150 * (m * m / s), 0, 0,
                                          1.7 * angstrom, 2);

    ((G4MoleculeDefinition*) anInstance)->SetLevelOccupation(0);
    ((G4MoleculeDefinition*) anInstance)->SetFormatedName(formatedName);
  }
  fDamagedDeoxyriboseEAQInstance = 
    reinterpret_cast<G4DNA_DamagedDeoxyriboseEAQ*>(anInstance);
  return fDamagedDeoxyriboseEAQInstance;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....
